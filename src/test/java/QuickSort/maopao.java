package QuickSort;

/**
 * @auther Siro
 * @create 2021/2/8
 */

/**
 * 冒泡排序
 */
public class maopao {

    public static int[] bubbleSort(int[] array){
        if(array.length ==0){
            return array;
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if(array[j+1]<array[j]){//<:asc >:Desc
                    int temp = array[j+1];
                    array[j+1]=array[j];
                    array[j]=temp;
                }
            }
        }
        return array;
    }

    private static String arrayToString(int[] arr,String flag) {
        String str = "数组为("+flag+")：";
        for(int a : arr) {
            str += a + "\t";
        }
        return str;
    }

    public static void main(String[] args) {
        int[] num = {2,34,50,12,60};
        System.out.println(arrayToString(num,"为排序"));

        maopao.bubbleSort(num);
        System.out.println(arrayToString(num,"排序"));
    }
}
