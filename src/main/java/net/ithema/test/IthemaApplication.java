package net.ithema.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @auther Siro
 * @create 2021/2/2
 */
@Slf4j
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class IthemaApplication {

    public static void main(String[] args) {
        SpringApplication.run(IthemaApplication.class, args);
        log.info("业务测试服务启动成功========================");
    }
}
